from flask import Flask, render_template, request

import requests

app = Flask(__name__)


@app.route('/')
def index():
    return render_template("login.html")

@app.route('/doLogin',methods=['POST'])
def doLogin():
   username = request.form['username']
   password = request.form['password']

   myData  = {"email":username,"psw":password}
   response = requests.post('http://127.0.0.1:5000/api/doLogin', json = myData)
   resp = response.json()

   if resp.get("loggedin") is True:
       token = resp.get("token")
       print(token)
       return resp.get("message")
   else:
       return "Login Failed"


@app.route('/myTreks')
def treks():
    response = requests.get('http://127.0.0.1:5000/api/treks')
    #print(response)
    #print(response.text)
    resp = response.json()

    return "All TREKS"


app.run(debug=True, port=5100)
